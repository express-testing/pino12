flash.ready(function() {

    var $input = document.querySelector('[data-search-keywords]');
    if($input === null) return;
    var keywords = flash.getUrlParameter('keywords');
    keywords = decodeURIComponent(keywords).replace(/\+/g, '%20');
    if(keywords === 'null' || !keywords) return;
    $input.value = keywords;

});