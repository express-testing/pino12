flashCore.prototype.currentBreakpoint = function() {
    var self = this;

    var breakpoint = null;
    var keys = Object.keys(self.breakpoints);

    keys.forEach(function(key, index){
        if(!breakpoint) {
            if(index === keys.length - 1 || (window.innerWidth >= self.breakpoints[key] && window.innerWidth < self.breakpoints[keys[index + 1]])) {
                breakpoint = key;
            }
        }
    });

    return breakpoint;
}