/**
 * Version: 1.0
 */

/**
 * Flash Core Library
 * This is a class with some utilities
 * In the frontend the object used is called "flash"
 */

var flashCore = function() {
	var self = this;
}

flashCore.prototype.breakpoints = {
    small: '0',
    medium: '640',
    large: '1024',
    container: '1230',
    xlarge: '1300'       
}

/**
 * Init the environment for a new page
 */
flashCore.prototype.newPage = function() {
	var self = this;

	// Page Init Functions
	var page_init = {
		callback: function(){
			// Init Features
			flash.scrollTo();
			flash.tabs();
			flash.dropdowns();
			flash.activeLinks();

			// Init the object fit fix for ie11
			flash.objectFitFix();
			flash.iosSrcsetFix();
			flash.initLazySizes();

			// Responsive menu
			if(document.querySelector('[data-flash-responsive-menu]')) {
				var flash_main_menu = new flashResponsiveMenu('[data-flash-responsive-menu]');
			}

			// Lightboxes
			var lightboxes = lightbox({trigger: '[data-lightbox]'});
			var lightboxes_forms = lightbox({trigger: '[data-lightbox-form]', trigger_attribute: 'data-lightbox-form', clone_content: false});
			var lightboxes_video = lightbox({trigger: '[data-lightbox-video]', trigger_attribute: 'data-lightbox-video'});
			
			/**
			 * Browsers Support
			 */
			// Not supported browser message
		    if(navigator.userAgent.match('MSIE 10.0;')) {
		    	document.body.insertAdjacentHTML('afterend', '<div class="browser_message" style="position: fixed; width: 100%; height: 100%; z-index: 1000000; top: 0; background: #142435; text-align: center; font-size: 0.8em; padding-top: 50px;"><div class="wrapper"><img style="width:195px;margin-bottom: 40px;" src="/assets/images/design/logo.png" alt="" Logo" /><h1 style="color: white;">Uh Oh! Your browser is too old to view our site.</h1><h2 style="color: white;">Here is what you need to do</h2><p style="color: white;">Please <a style="color:#D11368;" href="http://browsehappy.com/" target="_blank" rel="noopener">upgrade your browser</a> or install <a style="color:#D11368;" href="https://www.google.com/intl/en/chrome/browser/" target="_blank" rel="noopener">Google Chrome</a> to experience this website.</p></div></div><div class="overlay"></div>' );
			}
		}
	};

	// Page init also going back
	var page_init_always = {
		callback: function(){
			flash.selectSync();
		},
		always: true
	}

	self.startup_functions = {
		named: {},
		anonymous: {
			0: page_init,
			1: page_init_always
		}
	};
}

/**
 * Init the flash class and execute the code to init the environment
 * for a new page
 */
if(typeof flash === 'undefined') {
	var flash = new flashCore();
}
flash.newPage();